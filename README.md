**keywords:** 
<br>
Selenium, WebScraping, JavaSwing

**How the program functions:**
<br>
In resources folder, there is the CardsToSearch.properties file which lists all the card names that i want to search for. The program uses Selenium to open the browser, connect to one of the shops and search for each card on the list. When the results are displayed, it checks which of them are available in the shop and if they are, it saves all crucial data (exact name, price, amount, etc). After the search is finished, it connects to the next shop and repeats the previous steps. When all shops have been searched, the program creates an excel file with the data it collected.

**TODOs**
<br>
* [ ] Code refactoring
* [x] 3 browsers are used (opened and closed for each shop) - must be used only one (keep session alive)
* [x] GUI
* [x] additional capabilities of GUI (searching for only 1 card)
* [ ] additional capabilities of GUI (adding cards to list)
* [ ] additional capabilities of GUI (deleting cards from list)
* [x] make it an exe
* [ ] gathering the results in polish shops to be more specific
* [x] searching in mcm to be case insentive
* [ ] hide the browser
* [ ] checkboxes for search in specific shops
* [ ] fix the bug appending only half of the data collected to the excel file

**27/06/2019 notes:**
<br>
GUI with JavaSwing will be added at the end of the project. Its purpose is to add the possibility to add card names from its level (instead of editing the .properties file manually), as well as to set the path that the excel file will be saved.

**3/7/2019 notes:**
<br>
Alpha version has been finished. It can be started from command line running the main() in CardPriceChecker.This version works for all 3 original shops. The card names in the resource file must be very precise (using commas etc)

**22/7/2019 notes:**
<br>
Beta version has been finished. It is now an exe that presents GUI and searches for all cards or just a specific one. It can be built with maven and the exe file will be found in "target" folder with the name "CardpriceChecker-0.0.1-SNAPSHOT-jar-with-dependencies.jar". Double click it, few more options to choose from and voila, the excel file will be created on desktop.