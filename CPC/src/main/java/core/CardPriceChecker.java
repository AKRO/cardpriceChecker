package core;

import java.awt.EventQueue;

import gui.MainFrame;

/**
 * @author AKRO
 *
 */
public class CardPriceChecker {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					createGUI();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void createGUI() {
		MainFrame mainFrame = new MainFrame();
		mainFrame.prepareFrame();
	}

}
