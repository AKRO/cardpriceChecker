package shops;
/**
 *
 */

import static utils.Dictionary.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.*;

/**
 * @author AKRO
 *
 */
public class Flamberg {

	public ArrayList<String> flambergSearch(WebDriver browser, String[] listOfCards)
			throws IOException, URISyntaxException {
		ArrayList<String> cardList = new ArrayList<String>();

		browser.get(FLAMBERG_SITE);
		for (String cardToSearch : listOfCards) {
			WebElement resultTable = null;
			WebElement searchBar;
			WebElement searchButton;

			searchBar = browser.findElement(By.name(FLAMBERG_SEARCHBAR));
			searchBar.clear();
			searchBar.sendKeys(cardToSearch);
			searchButton = browser.findElement(By.cssSelector(FLAMBERG_SEARCHBUTTON));
			searchButton.click();

			try {
				resultTable = browser.findElement(By.id(FLAMBERG_RESULT_TABLE));
			} catch (NoSuchElementException e) {
				searchBar = browser.findElement(By.name(FLAMBERG_SEARCHBAR));
				searchBar.clear();
				searchBar.sendKeys(cardToSearch);
				searchButton = browser.findElement(By.cssSelector(FLAMBERG_SEARCHBUTTON));
				searchButton.click();
				try {
					resultTable = browser.findElement(By.id(FLAMBERG_RESULT_TABLE));
				} catch (NoSuchElementException e2) {
					continue;
				}
			}
			List<WebElement> rowVals = resultTable.findElements(By.tagName(FLAMBERG_TR));

			int rowNum = browser.findElements(By.xpath(FLAMBERG_ROWS)).size();
			int colNum = browser.findElements(By.xpath(FLAMBERG_COLUMNS)).size();

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < rowNum + 1; i++) {
				if (i != 0) {
					// Get each row's column values by tag name
					List<WebElement> colVals = rowVals.get(i).findElements(By.tagName(FLAMBERG_TD));
					// get the quantity and name column
					WebElement quantityResult = colVals.get(4);
					WebElement nameResult = colVals.get(0);

					if (!quantityResult.getText().equals(QUANTITY_ZERO)) {
						// if (nameResult.getText().trim().equals(cardToSearch.trim())) {

						for (int j = 0; j < colNum; j++) {
							// Save the column values to array
							sb.append(colVals.get(j).getText() + TOKEN);
							// System.out.print(colVals.get(j).getText() + "||");
						}
						cardList.add(sb.toString());
						sb.delete(0, sb.length() - 1);

						// }
					}
				}

			}
		}

		return cardList;

	}

}
