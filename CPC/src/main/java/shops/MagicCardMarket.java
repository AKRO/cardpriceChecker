package shops;

import static utils.Dictionary.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.*;

public class MagicCardMarket {
	static ArrayList<String> cardList = new ArrayList<String>();

	private static String cardName;
	private static String cardSet;
	private static String cardPrice;
	private static String cardPriceFoil;
	private static String cardFoil = Y;

	public ArrayList<String> mcmSearch(WebDriver browser, String[] listOfCards) throws IOException, URISyntaxException {

		browser.get(MCM_SITE);
		for (String cardToSearch : listOfCards) {
			WebElement searchBar;
			WebElement searchButton;
			WebElement resultTable;

			searchBar = browser.findElement(By.id(MCM_SEARCHBAR));
			searchBar.clear();
			searchBar.sendKeys(cardToSearch);
			searchButton = browser.findElement(By.id(MCM_SEARCHBUTTON));
			searchButton.click();
			try {
				resultTable = browser.findElement(By.cssSelector(MCM_RESULT_TABLE));
			} catch (NoSuchElementException e) {
				searchBar = browser.findElement(By.id(MCM_SEARCHBAR));
				searchBar.clear();
				searchBar.sendKeys(cardToSearch);
				searchButton = browser.findElement(By.id(MCM_SEARCHBUTTON));
				searchButton.click();
				try {
					resultTable = browser.findElement(By.cssSelector(MCM_RESULT_TABLE));
				} catch (NoSuchElementException n) {
					continue;
				}
			}
			List<WebElement> rowVals = resultTable.findElements(By.xpath(MCM_ROWS));
			int rowNum = rowVals.size();
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i < rowNum + 1; i++) {

				resultTable = browser.findElement(By.cssSelector(MCM_RESULT_TABLE));

				rowVals = resultTable.findElements(By.xpath(MCM_ROWS));
				WebElement webEle;
				try {
					webEle = resultTable.findElement(By.xpath(MCM_RESULT_TABLE_PART1 + i + MCM_RESULT_TABLE_PART2));
				} catch (NoSuchElementException e) {
					continue;
				}

				if (webEle.getText().trim().toLowerCase().startsWith(cardToSearch.trim().toLowerCase())) {
					webEle.click();
					List<WebElement> cardDetailsList;
					WebElement cardDetails;

					try {
						cardDetails = browser.findElement(By.xpath(MCM_CARD_DETAILS_V1));

					} catch (NoSuchElementException e) {
						cardDetails = browser.findElement(By.xpath(MCM_CARD_DETAILS_V2));

					}
					cardDetailsList = cardDetails.findElements(By.tagName(MCM_DT));

					// find and set the Name of the card
					WebElement cardNameOnSite;
					cardNameOnSite = browser.findElement(By.cssSelector(MCM_CARD_NAME_SELECTOR));
					cardName = cardNameOnSite.getText();

					// find and set the Edition of the card
					WebElement cardSetOnSite;
					try {
						cardSetOnSite = browser.findElement(By.xpath(MCM_CARD_SET_V1));
					} catch (NoSuchElementException e) {
						try {
							cardSetOnSite = browser.findElement(By.xpath(MCM_CARD_SET_V2));
						} catch (NoSuchElementException e2) {
							try {
								cardSetOnSite = browser.findElement(By.xpath(MCM_CARD_SET_V3));
							} catch (NoSuchElementException e3) {
								cardSetOnSite = browser.findElement(By.xpath(MCM_CARD_SET_V4));
							}
						}
					}
					cardSet = cardSetOnSite.getText();

					// find and set the Price of the card
					WebElement priceTrend;
					try {
						priceTrend = browser.findElement(By.xpath(MCM_CARD_PRICE_TREND_V1));
					} catch (NoSuchElementException e) {
						try {
							priceTrend = browser.findElement(By.xpath(MCM_CARD_PRICE_TREND_V2));
						} catch (NoSuchElementException e2) {
							continue;
						}
					}
					if (priceTrend != null) {
						cardPrice = priceTrend.getText();
					} else {
						cardPrice = MCM_TBA;
					}

					sb.append(cardName + TOKEN + cardSet + TOKEN + cardPrice);
					cardList.add(sb.toString());
					sb.delete(0, sb.length());

					// find and set the Foil Price of the card
					WebElement foilSwitchButtonOn;
					try {
						foilSwitchButtonOn = browser.findElement(By.xpath(MCM_FOIL_SWITCH_BUTTON_ON));
						foilSwitchButtonOn.click();
					} catch (NoSuchElementException e) {
						browser.navigate().back();
						continue;
					}
					WebElement priceTrendFoil = null;
					try {
						priceTrendFoil = browser.findElement(By.xpath(MCM_CARD_PRICE_TREND_FOIL_V1));
					} catch (NoSuchElementException e) {
						try {
							priceTrendFoil = browser.findElement(By.xpath(MCM_CARD_PRICE_TREND_FOIL_V2));
						} catch (NoSuchElementException e2) {
							continue;
						}
					}
					if (priceTrendFoil != null) {
						cardPriceFoil = priceTrendFoil.getText();
					} else {
						cardPriceFoil = MCM_TBA;
					}

					sb.append(cardName + TOKEN + cardSet + TOKEN + cardPriceFoil + TOKEN + cardFoil);
					cardList.add(sb.toString());
					sb.delete(0, sb.length());

					WebElement foilSwitchButtonOff;
					foilSwitchButtonOff = browser.findElement(By.xpath(MCM_FOIL_SWITCH_BUTTON_OFF));
					foilSwitchButtonOff.click();
					browser.navigate().back();
					browser.navigate().back();
					browser.navigate().back();
				}
			}

		}

		return cardList;
	}

}
