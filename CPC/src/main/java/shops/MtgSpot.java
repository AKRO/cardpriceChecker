package shops;

import static utils.Dictionary.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.*;

public class MtgSpot {

	public ArrayList<String> mtgSpotSearch(WebDriver browser, String[] listOfCards)
			throws IOException, InterruptedException, URISyntaxException {
		ArrayList<String> cardList = new ArrayList<String>();

		browser.get(MTGSPOT_SITE);
		Thread.sleep(3000);
		for (String cardToSearch : listOfCards) {
			WebElement resultTable = null;
			WebElement searchBar;
			WebElement searchButton;

			searchBar = browser.findElement(By.xpath(MTGSPOT_SEARCHBAR));
			searchBar.clear();
			searchBar.sendKeys(cardToSearch);
			searchButton = browser.findElement(By.xpath(MTGSPOT_SEARCHBUTTON));
			searchButton.click();

			try {
				resultTable = browser.findElement(By.xpath(MTGSPOT_RESULT_TABLE_V1));
			} catch (NoSuchElementException e) {
				searchBar = browser.findElement(By.xpath(MTGSPOT_SEARCHBAR));
				searchBar.clear();
				searchBar.sendKeys(cardToSearch);
				searchButton = browser.findElement(By.xpath(MTGSPOT_SEARCHBUTTON));
				searchButton.click();
				try {
					resultTable = browser.findElement(By.xpath(MTGSPOT_RESULT_TABLE_V2));
				} catch (NoSuchElementException n) {
					continue;
				}
			}

			List<WebElement> rowVals = resultTable.findElements(By.xpath(MTGSPOT_ROW_VAL));
			int rowNum = browser.findElements(By.xpath(MTGSPOT_ROWS)).size();
			int colNum = browser.findElements(By.xpath(MTGSPOT_COLUMNS)).size();

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < rowNum; i++) {
				if (i != 0) {
					List<WebElement> colVals = rowVals.get(i)
							.findElements(By.xpath(MTGSPOT_COL_VAL_PART1 + (i + 1) + MTGSPOT_COL_VAL_PART2));

					String quantityResult = colVals.get(1).getText();
					if (!quantityResult.equals(QUANTITY_ZERO)) {
						for (int j = 0; j < colNum; j++) {
							sb.append(colVals.get(j).getText().replace("\n", UNDERSCORE) + TOKEN);
						}
						cardList.add(sb.toString());
						sb.delete(0, sb.length() - 1);
					}

				}
			}

		}
		return cardList;

	}

}
