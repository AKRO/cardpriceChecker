package operations;

import static utils.Dictionary.NEW_LINE_CHAR;
import static utils.Dictionary.PROPERTIES_FILE_NAME;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;

public class CardListLoader {
	public String[] loadCardList() throws IOException, URISyntaxException {

		ClassLoader classLoader = CardListLoader.class.getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream(PROPERTIES_FILE_NAME);

		StringWriter writer = new StringWriter();
		IOUtils.copy(inputStream, writer, StandardCharsets.UTF_8);
		String content = writer.toString();

		String[] listOfCards = content.split(NEW_LINE_CHAR);

		return listOfCards;
	}

}
