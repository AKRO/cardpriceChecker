package operations;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;

import dto.CardDTO;
import shops.*;
import utils.ResultFileCreator;
import utils.StringToCardConverter;

public class CardSearcher {
	static WebDriver browser;

	public static void searchForDesiredCards(String[] listOfCards)
			throws InterruptedException, IOException, URISyntaxException {
		Flamberg flamberg = new Flamberg();
		MtgSpot mtgspot = new MtgSpot();
		MagicCardMarket magicCardMarket = new MagicCardMarket();
		ResultFileCreator excelCreator = new ResultFileCreator();
		StringToCardConverter stringToCardConverter = new StringToCardConverter();
		ArrayList<CardDTO> completeListToSearch = new ArrayList<CardDTO>();
		CardListLoader cardListLoader = new CardListLoader();
		String[] listToSearch = null;

		if (listOfCards != null && listOfCards[0] != "") {
			listToSearch = listOfCards;
		} else {
			listToSearch = cardListLoader.loadCardList();
		}

		browser = BrowserActions.startBrowser(browser);

		ArrayList<String> flambergStringTypeCardList = flamberg.flambergSearch(browser, listToSearch);
		ArrayList<String> mtgspotStringTypeCardList = mtgspot.mtgSpotSearch(browser, listToSearch);
		ArrayList<String> mcmStringTypeCardList = magicCardMarket.mcmSearch(browser, listToSearch);

		BrowserActions.stopBrowser(browser);

		ArrayList<CardDTO> flambergCardTypeCardList = stringToCardConverter
				.flambergConverter(flambergStringTypeCardList);
		ArrayList<CardDTO> mtgspotCardTypeCardList = stringToCardConverter.mtgSpotConverter(mtgspotStringTypeCardList);
		ArrayList<CardDTO> mcmCardTypeCardList = stringToCardConverter.mcmConverter(mcmStringTypeCardList);

		completeListToSearch.addAll(flambergCardTypeCardList);
		completeListToSearch.addAll(mtgspotCardTypeCardList);
		completeListToSearch.addAll(mcmCardTypeCardList);

		excelCreator.createExcelReport(completeListToSearch);

	}

}
