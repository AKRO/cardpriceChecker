package operations;

import java.io.IOException;
import java.net.URISyntaxException;

import org.openqa.selenium.WebDriver;

public class BrowserActions {

	public static WebDriver startBrowser(WebDriver browser) throws URISyntaxException, IOException {
		WebDriverSetup webDriverSetup = new WebDriverSetup();

		browser = webDriverSetup.prepareBrowser(browser);

		return browser;
	}

	public static void stopBrowser(WebDriver browser) {
		browser.quit();
	}
}
