package operations;

import static utils.Dictionary.WEBDRIVER_USED;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class WebDriverSetup {
	public WebDriver prepareBrowser(WebDriver browser) throws ZipException, URISyntaxException, IOException {
		FileExtractor fileExtractor = new FileExtractor();
		URI uri = fileExtractor.unzip();
		System.setProperty(WEBDRIVER_USED, uri.getPath());
		browser = new FirefoxDriver();
		browser.manage().timeouts().setScriptTimeout(3, TimeUnit.SECONDS);

		return browser;
	}

}
