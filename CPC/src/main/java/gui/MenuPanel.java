package gui;

import static utils.Dictionary.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MenuPanel extends JPanel implements ActionListener {
	private static final long serialVersionUID = -7586244907640270734L;
	PanelsAsCards panelsAsCards;
	JPanel buttonPanel;

	private JButton buttonSearch = new JButton(SEARCH_PANEL_BUTTON);
	private JButton buttonAdd = new JButton(ADD_PANEL_BUTTON);

	public MenuPanel(PanelsAsCards panelsAsCards) {
		this.panelsAsCards = panelsAsCards;
		createMenuPanel();
	}

	public void createMenuPanel() {
		setDesign();
		buttonSearch.addActionListener(this);
		buttonAdd.addActionListener(this);

	}

	private void setDesign() {
		setLayout(new BorderLayout());

		setButtonPanel();
		add(buttonPanel, BorderLayout.CENTER);

	}

	private void setButtonPanel() {
		buttonPanel = new JPanel();
		buttonPanel.setSize(127, 72);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));

		buttonSearch.setAlignmentX(Component.CENTER_ALIGNMENT);
		buttonAdd.setAlignmentX(Component.CENTER_ALIGNMENT);

		buttonPanel.add(buttonSearch);
		buttonPanel.add(Box.createRigidArea(new Dimension(0, 25)));
		buttonPanel.add(buttonAdd);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == buttonSearch) {
			SearchingPanel searchingPanel = new SearchingPanel(panelsAsCards);
			removeAll();
			add(searchingPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(SEARCHING);
		}
		if (e.getSource() == buttonAdd) {
			EditPanel editPanel = new EditPanel(panelsAsCards);
			removeAll();
			add(editPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(ADDITION);
		}
	}
}
