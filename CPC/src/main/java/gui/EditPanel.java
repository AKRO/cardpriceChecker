package gui;

import static utils.Dictionary.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;

import operations.AdditionToList;

public class EditPanel extends JPanel implements ActionListener, FocusListener {
	private static final long serialVersionUID = 8253404223389181410L;
	PanelsAsCards panelsAsCards;
	JPanel addCardTextFieldPanel;
	JPanel addCardButtonPanel;

	private JTextField additionTextField = new JTextField(TEXTFIELD);
	private JButton buttonAdd = new JButton(ADD_BUTTON);
	private JButton homeButton = new JButton(HOME_BUTTON);
	private JButton goToSearchingButton = new JButton(SWITCH_TO_SEARCH_PANEL_BUTTON);

	public EditPanel(PanelsAsCards panelsAsCards) {
		this.panelsAsCards = panelsAsCards;
		createAdditionPanel();
	}

	public void createAdditionPanel() {
		setDesign();
		additionTextField.setMaximumSize(new Dimension(500, 500));
		additionTextField.setHorizontalAlignment(SwingConstants.CENTER);
		additionTextField.addActionListener(this);
		additionTextField.addFocusListener(this);
		buttonAdd.addActionListener(this);
		homeButton.addActionListener(this);
		goToSearchingButton.addActionListener(this);

	}

	private void setDesign() {
		setLayout(new BorderLayout());
		addCardTextFieldPanel = new JPanel();
		addCardButtonPanel = new JPanel();
		addCardTextFieldPanel.setLayout(new BorderLayout());
		addCardButtonPanel.setLayout(new BorderLayout());

		addCardTextFieldPanel.add(additionTextField, BorderLayout.SOUTH);
		addCardButtonPanel.add(buttonAdd, BorderLayout.NORTH);

		JPanel navigationPanel = new JPanel();
		navigationPanel.setLayout(new BorderLayout());

		navigationPanel.add(homeButton, BorderLayout.LINE_END);
		navigationPanel.add(goToSearchingButton, BorderLayout.CENTER);

		JPanel fillerPanel = new JPanel();
		fillerPanel.setLayout(new BoxLayout(fillerPanel, BoxLayout.Y_AXIS));
		addCardTextFieldPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
		addCardButtonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);

		fillerPanel.add(addCardTextFieldPanel);
		fillerPanel.add(addCardButtonPanel);
		add(fillerPanel, BorderLayout.CENTER);
		add(navigationPanel, BorderLayout.PAGE_END);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == homeButton) {
			MenuPanel menuPanel = new MenuPanel(panelsAsCards);
			removeAll();
			add(menuPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(MENU);
		}
		if (e.getSource() == goToSearchingButton) {
			SearchingPanel searchingPanel = new SearchingPanel(panelsAsCards);
			removeAll();
			add(searchingPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(SEARCHING);
		}
		if (e.getSource() == buttonAdd) {
			AdditionToList additionToList = new AdditionToList();
			try {
				additionToList.addToList(additionTextField.getText());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		// if (e.getSource() == buttonDelete) {}

	}

	public void focusGained(FocusEvent e) {
		additionTextField.setText("");
	}

	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub

	}

}
