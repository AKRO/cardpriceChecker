package gui;

import static utils.Dictionary.*;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.*;

import operations.CardSearcher;

public class SearchingPanel extends JPanel implements ActionListener, FocusListener {
	private static final long serialVersionUID = 3828390521762030074L;
	String[] cardListOfOneCard = new String[1];

	PanelsAsCards panelsAsCards;
	JPanel searchOneTextFieldPanel;
	JPanel searchOneButtonPanel;

	private JTextField searchTextField = new JTextField(SEARCH_ONE_TEXTFIELD);
	private JButton searchOneButton = new JButton(SEARCH_ONE_BUTTON);
	private JButton searchAllButton = new JButton(SEARCH_ALL_BUTTON);
	private JButton homeButton = new JButton(HOME_BUTTON);
	private JButton goToAdditionButton = new JButton(SWITCH_TO_EDIT_PANEL_BUTTON);

	public SearchingPanel(PanelsAsCards panelsAsCards) {
		this.panelsAsCards = panelsAsCards;
		createSearchingPanel();
	}

	public void createSearchingPanel() {
		setDesign();

		searchTextField.addActionListener(this);
		searchTextField.addFocusListener(this);
		searchOneButton.addActionListener(this);
		searchAllButton.addActionListener(this);
		homeButton.addActionListener(this);
		goToAdditionButton.addActionListener(this);

	}

	private void setDesign() {
		setLayout(new BorderLayout());
		searchTextField.setMaximumSize(new Dimension(400, 25));
		JPanel navigationPanel = new JPanel();
		JPanel searchOnePanel = new JPanel();
		searchOnePanel.setMaximumSize(new Dimension(500, 500));
		JPanel searchAllPanel = new JPanel();
		searchOneTextFieldPanel = new JPanel();
		searchOneButtonPanel = new JPanel();
		searchAllPanel.setMaximumSize(new Dimension(300, 150));
		navigationPanel.setLayout(new BorderLayout());
		searchOnePanel.setLayout(new BoxLayout(searchOnePanel, BoxLayout.Y_AXIS));
		searchAllPanel.setLayout(new BoxLayout(searchAllPanel, BoxLayout.X_AXIS));

		searchAllButton.setMaximumSize(new Dimension(300, 50));
		searchAllButton.setAlignmentY(Component.CENTER_ALIGNMENT);

		searchOneTextFieldPanel.setLayout(new BorderLayout());
		searchOneButtonPanel.setLayout(new BorderLayout());
		searchOneTextFieldPanel.add(searchTextField, BorderLayout.SOUTH);
		searchOneButtonPanel.add(searchOneButton, BorderLayout.NORTH);

		searchOnePanel.add(searchOneTextFieldPanel);
		searchOnePanel.add(searchOneButtonPanel);
		searchAllPanel.add(searchAllButton, BorderLayout.EAST);
		navigationPanel.add(homeButton, BorderLayout.LINE_END);
		navigationPanel.add(goToAdditionButton, BorderLayout.CENTER);

		add(searchAllPanel, BorderLayout.EAST);
		add(searchOnePanel, BorderLayout.WEST);
		add(navigationPanel, BorderLayout.PAGE_END);

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == homeButton) {
			MenuPanel menuPanel = new MenuPanel(panelsAsCards);
			removeAll();
			add(menuPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(MENU);
		}
		if (e.getSource() == goToAdditionButton) {
			EditPanel additionPanel = new EditPanel(panelsAsCards);
			removeAll();
			add(additionPanel, BorderLayout.CENTER);
			revalidate();
			panelsAsCards.changePanels(ADDITION);
		}
		if (e.getSource() == searchOneButton) {
			cardListOfOneCard[0] = searchTextField.getText();
			try {
				CardSearcher.searchForDesiredCards(cardListOfOneCard);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			;
		}
		if (e.getSource() == searchAllButton) {
			try {
				CardSearcher.searchForDesiredCards(null);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			;
		}
	}

	public void focusGained(FocusEvent e) {
		searchTextField.setText("");

	}

	public void focusLost(FocusEvent e) {
		// TODO Auto-generated method stub

	}

}
