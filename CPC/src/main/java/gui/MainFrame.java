package gui;

import static utils.Dictionary.MENU;
import static utils.Dictionary.TITLE;

import javax.swing.JFrame;

public class MainFrame extends JFrame {
	public MainFrame() {
	}

	private static final long serialVersionUID = -8501365216981121525L;
	// private JPanel contentPane;

	public void prepareFrame() {
		setTitle(TITLE);
		setDefaultLookAndFeelDecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 300);
		setLocationRelativeTo(null);
		setVisible(true);
		setResizable(false);

		/*
		 * contentPane = new JPanel(); contentPane.setBorder(new EmptyBorder(10, 10, 10,
		 * 10)); contentPane.setLayout(new BorderLayout(0, 0));
		 * setContentPane(contentPane);
		 */

		PanelsAsCards panelsAsCards = new PanelsAsCards();
		MenuPanel menuPanel = new MenuPanel(panelsAsCards);
		getContentPane().add(menuPanel);
		panelsAsCards.changePanels(MENU);

	}

}
