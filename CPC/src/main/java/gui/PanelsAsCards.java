package gui;

import static utils.Dictionary.*;

import java.awt.CardLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class PanelsAsCards extends JPanel {

	private static final long serialVersionUID = -6275842433549978556L;
	MenuPanel menuPanel;
	EditPanel editPanel;
	SearchingPanel searchingPanel;

	JScrollPane menuCard, additionCard, searchingCard;

	public PanelsAsCards() {
		menuPanel = new MenuPanel(this);
		editPanel = new EditPanel(this);
		searchingPanel = new SearchingPanel(this);

		menuCard = new JScrollPane(menuPanel);
		additionCard = new JScrollPane(editPanel);
		searchingCard = new JScrollPane(searchingPanel);

		setLayout(new CardLayout());

		add(menuCard, MENU);
		add(additionCard, ADDITION);
		add(searchingCard, SEARCHING);
	}

	public void changePanels(String panelToShow) {
		CardLayout cl = (CardLayout) getLayout();
		cl.show(this, panelToShow);
	}

}
