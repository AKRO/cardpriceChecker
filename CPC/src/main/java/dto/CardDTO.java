package dto;

public class CardDTO {
	private String cardName;
	private String cardSet;
	private String cardQuality;
	private String cardPrice;
	private String cardQuantity;
	private String cardShop;
	private String cardFoil;

	public CardDTO(String name, String set, String quality, String price, String quantity, String shop, String foil) {
		this.cardName = name;
		this.cardSet = set;
		this.cardQuality = quality;
		this.cardPrice = price;
		this.cardQuantity = quantity;
		this.cardShop = shop;
		this.cardFoil = foil;
	}

	public String getCardName() {
		return this.cardName;
	}

	public String getCardSet() {
		return this.cardSet;
	}

	public String getCardQuality() {
		return this.cardQuality;
	}

	public String getCardPrice() {
		return this.cardPrice;
	}

	public String getCardQuantity() {
		return this.cardQuantity;
	}

	public String getCardShop() {
		return this.cardShop;
	}

	public String getCardFoil() {
		return this.cardFoil;
	}

}
