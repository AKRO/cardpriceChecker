package utils;

import java.util.ArrayList;

import dto.CardDTO;

public class StringToCardConverter {

	public ArrayList<CardDTO> flambergConverter(ArrayList<String> flambergStringList) {
		ArrayList<CardDTO> flambergCardTypeList = new ArrayList<CardDTO>();

		for (String cardAsString : flambergStringList) {
			flambergCardTypeList.add(StringManipulator.flambergStringToCard(cardAsString));
		}

		return flambergCardTypeList;
	}

	public ArrayList<CardDTO> mtgSpotConverter(ArrayList<String> mtgSpotStringList) {
		ArrayList<CardDTO> mtgspotCardTypeList = new ArrayList<CardDTO>();
		for (String cardAsString : mtgSpotStringList) {
			mtgspotCardTypeList.add(StringManipulator.mtgspotStringToCard(cardAsString));
		}

		return mtgspotCardTypeList;
	}

	public ArrayList<CardDTO> mcmConverter(ArrayList<String> mcmStringList) {
		ArrayList<CardDTO> mcmCardTypeList = new ArrayList<CardDTO>();
		for (String cardAsString : mcmStringList) {
			mcmCardTypeList.add(StringManipulator.mcmStringToCard(cardAsString));
		}
		return mcmCardTypeList;
	}
}
