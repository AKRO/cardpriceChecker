package utils;

import static utils.Dictionary.*;

import java.util.StringTokenizer;

import dto.CardDTO;

public class StringManipulator {
	static StringTokenizer strTok;

	public static CardDTO flambergStringToCard(String cardValues) {
		strTok = new StringTokenizer(cardValues);
		String tempName = strTok.nextToken(TOKEN);
		String tempSet = strTok.nextToken(TOKEN);
		String tempQuality = strTok.nextToken(TOKEN);
		String tempPrice = strTok.nextToken(TOKEN);
		String tempQuantity = strTok.nextToken(TOKEN);
		String tempFoil = EMPTY;
		if (strTok.nextToken().equals(FLAMBERG_FOIL)) {
			tempFoil = Y;
		}

		CardDTO cardFromString = new CardDTO(tempName, tempSet, tempQuality, tempPrice, tempQuantity, FLAMBERG,
				tempFoil);
		return cardFromString;
	}

	public static CardDTO mcmStringToCard(String cardValues) {
		strTok = new StringTokenizer(cardValues);
		String tempName = strTok.nextToken(TOKEN);
		String tempSet = strTok.nextToken(TOKEN);
		String tempQuality = NM;
		String tempPrice = strTok.nextToken(TOKEN);
		String tempQuantity = AMOUNT;
		String tempFoil = EMPTY;
		if (strTok.hasMoreTokens()) {
			tempFoil = Y;
		}

		CardDTO cardFromString = new CardDTO(tempName, tempSet, tempQuality, tempPrice, tempQuantity, MCM, tempFoil);
		return cardFromString;
	}

	public static CardDTO mtgspotStringToCard(String cardValues) {
		strTok = new StringTokenizer(cardValues);
		String tempName = strTok.nextToken(TOKEN);
		String tempQuantity = strTok.nextToken(TOKEN);
		String tempPrice = strTok.nextToken(TOKEN);
		String tempSet;
		String tempQuality;
		String tempFoil = EMPTY;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(EMPTY);
			sb.append(tempName.substring(tempName.indexOf(UNDERSCORE), tempName.lastIndexOf(COMMA)));
			sb.deleteCharAt(0);
			tempSet = sb.toString();
		} catch (StringIndexOutOfBoundsException e1) {
			tempSet = UNKNOWN;
			tempQuality = UNKNOWN;
		}
		tempQuality = tempName.substring(0, 2);
		try {
			tempName = tempName.substring(3, tempName.indexOf(UNDERSCORE));

		} catch (StringIndexOutOfBoundsException e2) {
			tempQuality = UNKNOWN;
		}
		if (tempName.contains(FOIL)) {
			tempName = tempName.replace(FOIL, EMPTY);
			tempFoil = Y;

		}
		if (tempName.contains(STARS)) {
			tempName = tempName.replace(STARS, EMPTY);
		}
		CardDTO cardFromString = new CardDTO(tempName, tempSet, tempQuality, tempPrice, tempQuantity, MTGSPOT,
				tempFoil);
		return cardFromString;
	}

}
