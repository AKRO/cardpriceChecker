package utils;

import static utils.Dictionary.*;

import java.io.*;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import dto.CardDTO;

public class ResultFileCreator {

	private static String[] columns = { "Name", "Set", "Quality", "Price", "Quantity", "Shop", "Foil" };

	public void createExcelReport(ArrayList<CardDTO> cardListToSearch) throws IOException {

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet(SHEET_NAME);

		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setColor(IndexedColors.BLUE_GREY.getIndex());

		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);

		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
			cell.setCellStyle(headerCellStyle);
		}

		int rowNum = 1;
		for (CardDTO card : cardListToSearch) {

			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(card.getCardName().trim());
			row.createCell(1).setCellValue(card.getCardSet());
			row.createCell(2).setCellValue(card.getCardQuality());
			row.createCell(3).setCellValue(card.getCardPrice());
			row.createCell(4).setCellValue(card.getCardQuantity());
			row.createCell(5).setCellValue(card.getCardShop());
			row.createCell(6).setCellValue(card.getCardFoil());
		}
		for (int i = 0; i < columns.length; i++) {
			sheet.autoSizeColumn(i);
		}

		File desktopDir = new File(System.getProperty(USER_HOME), DESKTOP);
		// for some reason this overwrites some data(rows) and appends the rest -.-'
		// if i change the boolean to true it crashes the excel file
		FileOutputStream fileOut = new FileOutputStream(new File(desktopDir, OUTPUT_FILE_NAME));
		workbook.write(fileOut);
		workbook.close();
		fileOut.close();

		JOptionPane.showMessageDialog(null, ENDING_CREDITS);
	}

}
