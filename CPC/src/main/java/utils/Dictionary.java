package utils;

public class Dictionary {
	// General
	public static final String WEBDRIVER_USED = "webdriver.gecko.driver";
	public static final String WEBDRIVER_NAME = "geckodriver.exe";

	public static final String PROPERTIES_FILE_NAME = "CardsToSearch.properties";
	public static final String NEW_LINE_CHAR = "\\n";
	public static final String TOKEN = "||";
	public static final String QUANTITY_ZERO = "0";
	public static final String Y = "Y";
	public static final String USER_HOME = "user.home";
	public static final String DESKTOP = "Desktop";
	public static final String ENDING_CREDITS = "Search finished. Excel file created on DESKTOP.";

	// Flamberg
	public static final String FLAMBERG = "flamberg";
	public static final String FLAMBERG_SITE = "http://flamberg.com.pl/";
	public static final String FLAMBERG_SEARCHBAR = "keywords";
	public static final String FLAMBERG_SEARCHBUTTON = ".input-group > span:nth-child(2)";
	public static final String FLAMBERG_RESULT_TABLE = "productListing";
	public static final String FLAMBERG_ROWS = "/html/body/div[2]/div/div[2]/div[1]/div[3]/div[1]/table/tbody/tr";
	public static final String FLAMBERG_COLUMNS = "//table[@id='productListing']/tbody/tr[1]/td";
	public static final String FLAMBERG_TR = "tr";
	public static final String FLAMBERG_TD = "td";
	public static final String FLAMBERG_FOIL = "FOIL";

	// MtgSpot
	public static final String MTGSPOT = "mtgSpot";
	public static final String MTGSPOT_SITE = "https://mtgspot.pl/";
	public static final String MTGSPOT_SEARCHBAR = "/html/body/div/header/div[2]/form/div[1]/div/input";
	public static final String MTGSPOT_SEARCHBUTTON = "//*[@id=\"searcher\"]/div[1]/div/button";
	public static final String MTGSPOT_RESULT_TABLE_V1 = "//*[@id=\"products-list\"]/div[2]";
	public static final String MTGSPOT_RESULT_TABLE_V2 = "//*[@id=\"products-list\"]/div";
	public static final String MTGSPOT_ROW_VAL = "//*[@id=\"products-list\"]/div[2]/div";
	public static final String MTGSPOT_ROWS = "//*[@id=\"products-list\"]/div[2]/div";
	public static final String MTGSPOT_COLUMNS = "//*[@id=\"products-list\"]/div[2]/div[1]/div";
	public static final String MTGSPOT_COL_VAL_PART1 = "//*[@id=\"products-list\"]/div[2]/div[";
	public static final String MTGSPOT_COL_VAL_PART2 = "]/div";

	// MagicCardMarket
	public static final String MCM = "mcm";
	public static final String MCM_SITE = "https://www.cardmarket.com/en/Magic";
	public static final String MCM_SEARCHBAR = "ProductSearchInput";
	public static final String MCM_SEARCHBUTTON = "search-btn";
	public static final String MCM_RESULT_TABLE = ".table-body";
	public static final String MCM_RESULT_TABLE_PART1 = "/html/body/main/section/div[3]/div[2]/div[";
	public static final String MCM_RESULT_TABLE_PART2 = "]/div[4]/div/div[1]";
	public static final String MCM_FOIL_SWITCH_BUTTON_ON = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[1]/label/span[1]";
	public static final String MCM_FOIL_SWITCH_BUTTON_OFF = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[1]/label/span[1]";
	public static final String MCM_TBA = "TBA";
	public static final String MCM_ROWS = "/html/body/main/section/div[3]/div[2]/div";
	public static final String MCM_CARD_DETAILS_V1 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[2]/div/div[2]/dl";
	public static final String MCM_CARD_DETAILS_V2 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[1]/div/div[2]/dl";
	public static final String MCM_CARD_NAME_SELECTOR = "li.breadcrumb-item:nth-child(5) > a:nth-child(1) > span:nth-child(1)";
	public static final String MCM_CARD_SET_V1 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[2]/div/div[2]/dl/dd[3]/div/a[2]";
	public static final String MCM_CARD_SET_V2 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[1]/div/div[2]/dl/dd[3]/div/a[2]";
	public static final String MCM_CARD_SET_V3 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[1]/div/div[2]/dl/dd[2]/div/a[2]";
	public static final String MCM_CARD_SET_V4 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[2]/div/div[2]/dl/dd[2]/div/a[2]";
	public static final String MCM_CARD_PRICE_TREND_V1 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[2]/div/div[2]/dl/dd[7]/span";
	public static final String MCM_CARD_PRICE_TREND_V2 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[1]/div/div[2]/dl/dd[7]/span";
	public static final String MCM_CARD_PRICE_TREND_FOIL_V1 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[2]/div/div[2]/dl/dd[7]/span";
	public static final String MCM_CARD_PRICE_TREND_FOIL_V2 = "/html/body/main/div[3]/section[2]/div/div[2]/div[1]/div/div[1]/div/div[2]/dl/dd[7]/span";
	public static final String MCM_DT = "dt";

	// ResultFileCreator
	public static final String SHEET_NAME = "Cards";
	public static final String OUTPUT_FILE_NAME = "CardList.xlsx";

	// FileExtractor
	public static final String CANNOT_FIND_FILE = "cannot find file: ";
	public static final String IN_ARCHIVE = " in archive: ";

	// StringManipulator
	public static final String UNKNOWN = "UNKNOWN";
	public static final String FOIL = "Foil";
	public static final String EMPTY = "";
	public static final String STARS = "***";
	public static final String UNDERSCORE = "_";
	public static final String COMMA = ",";
	public static final String NM = "NM";
	public static final String AMOUNT = "3016";

	// ** GUI **
	public static final String HOME_BUTTON = "HOME";
	// AdditionPanel
	public static final String TEXTFIELD = "Type name of card...";
	public static final String ADD_BUTTON = "Add this card to the list.";
	public static final String SWITCH_TO_SEARCH_PANEL_BUTTON = "Go to searching panel.";
	// MainFrame
	public static final String TITLE = "MTG card price checker";
	// MenuPanel
	public static final String SEARCH_PANEL_BUTTON = "Search for cards";
	public static final String ADD_PANEL_BUTTON = "Add cards to list";
	// PanelsAsCards
	public final static String MENU = "Menu";
	public final static String ADDITION = "Addition";
	public final static String SEARCHING = "Searching";
	// SearchingPanel
	public static final String SEARCH_ONE_TEXTFIELD = "Search for a specific card...";
	public static final String SEARCH_ONE_BUTTON = "Search only for this card.";
	public static final String SEARCH_ALL_BUTTON = "Search for all cards on the list.";
	public static final String SWITCH_TO_EDIT_PANEL_BUTTON = "Go to edition panel.";
}
